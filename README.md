<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Thanks again! Now go create something AMAZING! :D
***
***
***
*** To avoid retyping too much info. Do a search and replace for the following:
*** github_username, repo_name, twitter_handle, email, project_title, project_description
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
<!-- [![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url] -->



<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/giantsweetroll/AI-Roulette">
    <img src="images/logo.png" alt="Logo" width="15%">
  </a>
  <h3 align="center">AI Roulette</h3>
  <p>
    A party game where you let an AI decide your fate. Made using Python.
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
    <li><a href="#screenshots">Screenshots</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

[![AI Roulette Screen Shot][product-screenshot]](https://gitlab.com/giantsweetroll/AI-Roulette)

This is my final project for Introduction to Programming and Program Design Methods course. It is a game written in Python intended to be played with other people as a party game (locally). The players would take turn picking an AI and would draw a character `0-9` or `A-Z` and let the AI try to identify it.


### Built With

* [Python](https://www.python.org/)


<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

* [Python](https://www.python.org/)

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/giantsweetroll/AI-Roulette.git
   ```
2. Install Python packages (Using a venv is highly recommended)
   ```sh
   pip install numpy matplotlib opencv-python tensorflow pygame
   ```



<!-- USAGE EXAMPLES -->
## Usage
Open a terminal in the project directory and type
```
python main.py
```
This will start the application. It might take a while before the game window appears.

Instructions on how to play can be viewed by pressing the "Instructions" button on the main menu.
<br />
![Instructions Screen Shot][instructions-screenshot]

<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.



<!-- CONTACT -->
## Contact

Gardyan Priangga Akbar - [LinkedIn](https://www.linkedin.com/in/gardyanakbar/) - gardyan.akbar@binus.ac.id

Project Link: [https://gitlab.com/giantsweetroll/AI-Roulette](https://gitlab.com/giantsweetroll/AI-Roulette)



<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements

* [Matplotlib](https://matplotlib.org/)
* [Numpy](https://numpy.org/)
* [OpenCV](https://pypi.org/project/opencv-python/)
* [Pygame](https://www.pygame.org/news)
* [TensorFlow](https://www.tensorflow.org/)


<!-- SCREENSHOTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Screenshots</h2></summary>
  <img src="images/mainmenu.PNG" alt="main menu" width="20%">
  <img src="images/gamemode_selection.PNG" alt="gamemode selection" width="20%">
  <img src="images/ai_selection.PNG" alt="AI selection screen" width="20%">
  <img src="images/loading_screen.PNG" alt="loading screen" width="20%">
  <img src="images/result.PNG" alt="result screen" width="20%">
  <img src="images/random_chaos.PNG" alt="random chaos" width="20%">
  <img src="images/instructions.PNG" alt="instructions screen" width="20%">
</details>


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/github_username/repo.svg?style=for-the-badge
[contributors-url]: https://github.com/github_username/repo/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/github_username/repo.svg?style=for-the-badge
[forks-url]: https://github.com/github_username/repo/network/members
[stars-shield]: https://img.shields.io/github/stars/github_username/repo.svg?style=for-the-badge
[stars-url]: https://github.com/github_username/repo/stargazers
[issues-shield]: https://img.shields.io/github/issues/github_username/repo.svg?style=for-the-badge
[issues-url]: https://github.com/github_username/repo/issues
[license-shield]: https://img.shields.io/github/license/github_username/repo.svg?style=for-the-badge
[license-url]: https://github.com/github_username/repo/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/github_username
[product-screenshot]: images/screenshot.png
[instructions-screenshot]: images/instructions.PNG
[gamemode-screenshot]: images/gamemode_selection.PNG
[draw-screenshot]: images/draw_l.PNG
[result-screenshot]: images/result.PNG
