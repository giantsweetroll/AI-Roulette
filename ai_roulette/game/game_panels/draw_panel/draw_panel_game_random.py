from ai_roulette.game import game_functions, \
    global_vars
from ai_roulette.game.game_panels.draw_panel.draw_panel_game import DrawPanelGame
from ai_roulette.game.settings import Settings


class DrawPanelGameRandom(DrawPanelGame):
    
    #Constructor
    def __init__(self, screen, settings:Settings):
        super().__init__(screen, settings)
    
    #Overridden Methods
    def check_guess_button(self, mouse_pos:()):
        """Go to loading screen"""
        global_vars.panel_index = 3
        super().check_guess_button(mouse_pos)
        global_vars.active_ai.set_name("AI")
    
    def check_exit_button(self, mouse_pos:()):
        """Go to previous panel"""
        if game_functions.mouse_on_button(self.get_exit_button(), mouse_pos):
            global_vars.panel_index -= 1