from ai_roulette import constants
from ai_roulette.game.misc.image_panel import ImagePanel


class AIInfoDisplay(ImagePanel):
    
    #Constructor
    def __init__(self, path=constants.path_img_ai_info_empty):
        super().__init__(path)